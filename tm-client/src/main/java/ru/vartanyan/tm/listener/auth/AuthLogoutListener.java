package ru.vartanyan.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractAuthListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;

@Component
public class AuthLogoutListener extends AbstractAuthListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout";
    }

    @Override
    @EventListener(condition = "@authLogoutListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[LOGOUT]");
        sessionEndpoint.closeSession(session);
    }

}
