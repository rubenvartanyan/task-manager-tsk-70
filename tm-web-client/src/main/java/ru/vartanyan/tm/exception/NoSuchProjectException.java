package ru.vartanyan.tm.exception;

public class NoSuchProjectException extends AbstractException {

    public NoSuchProjectException() {
        super("Error! No such project found...");
    }

}
